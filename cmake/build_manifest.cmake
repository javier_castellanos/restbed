# Copyright (c) 2013, 2014, 2015 Corvusoft

set( MANIFEST
     ${SOURCE_DIR}/settings.cpp
     ${SOURCE_DIR}/detail/settings_impl.cpp
     ${SOURCE_DIR}/status_code.cpp
     ${SOURCE_DIR}/detail/status_code_impl.cpp
     ${SOURCE_DIR}/method.cpp
     ${SOURCE_DIR}/detail/method_impl.cpp
     ${SOURCE_DIR}/resource.cpp
     ${SOURCE_DIR}/detail/resource_impl.cpp
     ${SOURCE_DIR}/request.cpp
     ${SOURCE_DIR}/detail/request_impl.cpp
     ${SOURCE_DIR}/detail/request_builder_impl.cpp
     ${SOURCE_DIR}/response.cpp
     ${SOURCE_DIR}/detail/response_impl.cpp
     ${SOURCE_DIR}/service.cpp
     ${SOURCE_DIR}/detail/service_impl.cpp
     ${SOURCE_DIR}/logger.cpp
     ${SOURCE_DIR}/detail/logger_impl.cpp
     ${SOURCE_DIR}/detail/resource_matcher_impl.cpp
     ${SOURCE_DIR}/detail/path_parameter_impl.cpp
)
